from six import iteritems
import logging
from functools import partial
import numpy as np

import psana
import PSCalib.GlobalUtils as gu

log = logging.getLogger('smalldata')

_damage_on = 1
_damage_off = 0


def check_detector_present(env, name):
    """utility function to check for the presence of a detector in the psana.Env object

    Args:
        env: psana.Env object
        name: detector name to find (e.g. 'Epix100a')

    Returns:
        True if the detector is found in the run, False otherwise.

    """
    aliases = [key.alias() for key in env.configStore().keys()]
    if name in aliases:
        log.debug('found `{}` in detector aliases'.format(name))
        return True

    sources = [repr(key.src()) for key in env.configStore().keys()]
    if name in sources:
        log.debug('found `{}` in detector sources'.format(name))
        return True

    epics_vars = psana.DetNames('epics', env)
    fullname = list(map(lambda x: x[0], epics_vars))
    daqname = list(map(lambda x: x[1], epics_vars))
    username = list(map(lambda x: x[2], epics_vars))
    if any([name in fullname, name in daqname, name in username]):
        log.debug('found `{}` in epics variables'.format(name))
        return True

    det_vars = psana.DetNames('detectors', env)
    fullname = list(map(lambda x: x[0], det_vars))
    daqname = list(map(lambda x: x[1], det_vars))
    username = list(map(lambda x: x[2], det_vars))
    if any([name in fullname, name in daqname, name in username]):
        log.debug('found `{}` in detnames variables'.format(name))
        return True

    log.info('did not find detector {} in current environment'.format(name))
    return False


def process_default_detectors(evt, hutch='xpp'):
    """get the commonly-used detector data from a hutch

    Args:
        evt: an event object

    Keyword Args:
        hutch: a lowercase-string representing the hutch

    Returns:
        a dictionary containing detector names and patches

    """
    #dlst = sdt.defaultDetectors(hutch)
    #log.debug('added default detectors {!s}'.format(dlst))
    #data = sdt.detData(dlst, evt)
    eid = evt.get(psana.EventId) # potentially add additional info from event id
    data = dict(time_tuple=np.array(eid.time(), dtype=int), damage=_damage_off)

    return data


def make_control_data(env, run):
    """construct the control data reader

    Args:
        env:
        run:

    Returns:

    """
    log.debug('making control data detector')
    det = psana.Detector('ControlData', env)
    par = partial(_process_control_data, det=det)
    return [par,]


def _process_control_data(evt, det=None):
    """obtain control data keys

    Args:
        evt: event

    Keyword Args:
        det: preinitialized detector object

    Returns:

    """
    data = {}
    cfg = det()
    for control in cfg.pvControls():
        data.update({control.name(): control.value()})

    log.debug('writing control data %s', data)
    return dict(ControlData=data)


def make_epics_recorder(env, run, varlist=None, detname='epics'):
    """make epics processor

    Args:
        env:
        run:
        varlist:

    Returns:

    """
    vardet = {}
    for name in filter(lambda n: check_detector_present(env, n), varlist):
        det = psana.Detector(name, env)
        vardet.update({name: det})

    par = partial(_process_epics_vars, vardet=vardet, detname=detname)
    return [par,]


def _process_epics_vars(evt, vardet=None, detname='epics'):
    """

    Args:
        evt:

    Keyword Args:
        varlist: a list of epics vars for

    Returns:

    """
    data = {}

    for name, det in iteritems(vardet):
        data.update({name: det()})

    return {detname: data}

def make_feespec_detectors(env, run, aliases=None):
    log.debug('making feespec detector')

    lst = []
    for name in filter(lambda _n: check_detector_present(env, _n), aliases):
        name = name.encode('ascii')
        ddl = psana.Detector(name)
        par = partial(_process_feespec_detector, det=ddl, name=name)
        lst.append(par)

    return lst

def _process_feespec_detector(evt, det, name):
    log.debug('in feespec ({detname}) processing'.format(detname=name))

    spec = det.get(evt)
    keyname = '{}/hproj'.format(name)
    d = {keyname: spec.hproj()}

    return d

def make_jungfrau(env, run, aliases=None):
    """configures a jungfrau camera given a ds environment

    Args:
        env: a DataSource environment
        run: run number to get the calibration for

    Keyword Args:
        aliases: list of aliases to add

    Returns:
        a list of analysis processing functions to add to the first_pass list

    """
    lst = []

    for name in filter(lambda _n: check_detector_present(env, _n), aliases):
        det = psana.Detector(name)
        cfg = env.configStore().get(psana.Jungfrau.ConfigV3, det.source) # FIXME: store interesting configs from here
        mask = det.mask(run, calib=True, status=False, edges=True, central=True, unbond=True, unbondnbrs=True)
        gain = det.gain(run) # FIXME: should I be using this pixel gain?
        status = det.loading_status(run, gu.PIXEL_GAIN)

        # FIXME: avoid dependency on MPI rank by defering decision
        #if status != 1 and rank == 0:
        #    log.info('cannot find global gain file for {}'.format(name))
        #    #comm.Abort(-1)

        par = partial(_process_jungfrau, det=det, name=name, mask=mask)
        lst.append(par)

    return lst


def _process_jungfrau(evt, det=None, name=None, mask=None):
    """processes a jungfrau frame

    Args:
        evt: event

    Keyword Args:
        det: detector object to read from
        name: detector to look up
        mask: precached mask, will be looked up otherwise

    Returns:
        data from the frame

    """
    d = {}
    log.debug('in jungfrau ({detname}) processing, obtaining calibrated image'.format(detname=name))
    img = det.calib(evt)

    if img is None:
        log.warn('({detname}) reported a missing shot'.format(detname=name))
        return {'damage': _damage_on} # data is missing, use backfilling later

    # apply a mask to the image
    img *= mask

    d.update({'{}/image'.format(name): img})

    return d

def make_rayonix(env, run, aliases=None):
    """configures a rayonix camera given a ds environment

    Args:
        env: a DataSource environment
        run: run number to get the calibration for

    Keyword Args:
        aliases: list of aliases to add

    Returns:
        a list of analysis processing functions to add to the first_pass list

    """
    lst = []

    for name in filter(lambda _n: check_detector_present(env, _n), aliases):
        det = psana.Detector(name)
        cfg = env.configStore().get(psana.Rayonix.ConfigV2, det.source) # FIXME: store interesting configs from here
        mask = det.mask(run, calib=True, status=False, edges=True, central=True, unbond=True, unbondnbrs=True)
        gain = det.gain(run) # FIXME: should I be using this pixel gain?
        status = det.loading_status(run, gu.PIXEL_GAIN)

        # FIXME: avoid dependency on MPI rank by defering decision
        #if status != 1 and rank == 0:
        #    log.info('cannot find global gain file for {}'.format(name))
        #    #comm.Abort(-1)

        par = partial(_process_rayonix, det=det, name=name, mask=mask)
        lst.append(par)

    return lst


def _process_rayonix(evt, det=None, name=None, mask=None):
    """processes a jungfrau frame

    Args:
        evt: event

    Keyword Args:
        det: detector object to read from
        name: detector to look up
        mask: precached mask, will be looked up otherwise

    Returns:
        data from the frame

    """
    d = {}
    log.debug('in rayonix ({detname}) processing, obtaining calibrated image'.format(detname=name))
    img = det.calib(evt)

    if img is None:
        log.warn('({detname}) reported a missing shot'.format(detname=name))
        return {'damage': _damage_on} # data is missing, use backfilling later

    # apply a mask to the image
    img *= mask

    d.update({'{}/image'.format(name): img})

    return d


def make_epix100a(env, run, aliases=None):
    """configures an epix100a camera given a ds environment

    Args:
        env: a DataSource environment
        run: run number to get the calibration for

    Keyword Args:
        aliases: list of aliases to add

    Returns:
        a list of analysis processing functions to add to the first_pass list

    """
    lst = []

    for name in filter(lambda _n: check_detector_present(env, _n), aliases):
        det = psana.Detector(name)
        cfg = env.configStore().get(psana.Epix.Config100aV2, det.source) # FIXME: store interesting configs from here
        mask = det.mask(run, calib=True, status=False, edges=True, central=True, unbond=True, unbondnbrs=True)
        gain = det.gain(run) # FIXME: should I be using this pixel gain?
        status = det.loading_status(run, gu.PIXEL_GAIN)

        # FIXME: avoid dependency on MPI rank by defering decision
        #if status != 1 and rank == 0:
        #    log.info('cannot find global gain file for {}'.format(name))
        #    #comm.Abort(-1)

        par = partial(_process_epix100a, det=det, name=name, mask=mask)
        lst.append(par)

    return lst


def _process_epix100a(evt, det=None, name=None, mask=None):
    """processes an epix100a frame

    Args:
        evt: event

    Keyword Args:
        det: detector object to read from
        name: detector to look up
        mask: precached mask, will be looked up otherwise

    Returns:
        data from the frame

    """
    d = {}
    log.debug('in epix100a ({detname}) processing, obtaining calibrated image'.format(detname=name))
    img = det.calib(evt)

    if img is None:
        log.warn('({detname}) reported a missing shot'.format(detname=name))
        return {'damage': _damage_on} # data is missing, use backfilling later

    # apply a mask to the image
    img *= mask

    d.update({'{}/image'.format(name): img})

    return d


def make_cspad(env, run, aliases=None):
    """configures cspad cameras given a ds environment

    Args:
        env: a DataSource environment
        run: run number to get the calibration for

    Keyword Args:
        aliases: list of aliases to add

    Returns:
        a list of analysis processing functions to add to the first_pass list

    """
    lst = []

    for name in filter(lambda _n: check_detector_present(env, _n), aliases):
        det = psana.Detector(name)
        #cfg = env.configStore().get(psana.Epix.Config100aV2, det.source) # FIXME: consider storing something from cspad
        mask = det.mask(run, calib=True, status=False, edges=True, central=True, unbond=True, unbondnbrs=True)
        gain = det.gain(run) # FIXME: should I be using this pixel gain?
        status = det.loading_status(run, gu.PIXEL_GAIN)

        # FIXME: avoid dependence on MPI rank
        #if status != 1 and rank == 0:
        #    log.info('cannot find global gain file for {}'.format(name))
        #    #comm.Abort(-1)

        par = partial(_process_cspad, det=det, name=name, mask=mask)
        lst.append(par)

    return lst


def _process_cspad(evt, det=None, name=None, mask=None):
    """processes an cspad frame

    Args:
        evt: event

    Keyword Args:
        det: detector object to read from
        name: detector to look up
        mask: precached mask, will be looked up otherwise

    Returns:
        data from the frame

    """
    d = {}
    log.debug('in cspad ({detname}) processing, obtaining calibrated image'.format(detname=name))
    img = det.calib(evt)

    if img is None:
        log.warn('({detname}) reported a missing shot'.format(detname=name))
        return {'damage': _damage_on} # data is missing, use backfilling later

    # apply a mask to the image
    img *= mask

    # FIXME: determine how best to stack the two tiles of the detector
    img = np.concatenate([img[0], img[1]], axis=0)

    d.update({'{}/image'.format(name): img})

    return d


def make_zyla(env, run, aliases=None):
    """configures an zyla camera given a ds environment

    Args:
        env: a DataSource environment
        run: run number to get the calibration for

    Keyword Args:
        aliases: list of aliases to add

    Returns:
        a list of analysis processing functions to add to the first_pass list

    """
    lst = []

    for name in filter(lambda _n: check_detector_present(env, _n), aliases):
        det = psana.Detector(name)
        cfg = env.configStore().get(psana.Zyla.ConfigV1, det.source) # FIXME: store interesting configs from here
        mask = det.mask(run, calib=True, status=False, edges=True, central=True, unbond=True, unbondnbrs=True)
        gain = det.gain(run) # FIXME: should I be using this pixel gain?
        status = det.loading_status(run, gu.PIXEL_GAIN)

        par = partial(_process_zyla, det=det, name=name, mask=mask)
        lst.append(par)

    return lst


def _process_zyla(evt, det=None, name=None, mask=None):
    """processes a zyla frame

    Args:
        evt: event

    Keyword Args:
        det: detector object to read from
        name: detector to look up
        mask: precached mask, will be looked up otherwise

    Returns:
        data from the frame

    """
    d = {}
    log.debug('in zyla ({detname}) processing, obtaining calibrated image'.format(detname=name))
    img = det.calib(evt)

    if img is None:
        log.warn('({detname}) reported a missing shot'.format(detname=name))
        return {'damage': _damage_on} # data is missing, use backfilling later

    # apply a mask to the image
    if mask is not None:
        img *= mask

    d.update({'{}/image'.format(name): img})

    return d


def make_ipms(env, run, aliases=None):
    """adds functions to process ipms

    Args:
        env:
        run:

    Keyword Args:
        aliases: a list of ipm aliases

    Returns:
        a list of analysis processing functions to add to the first_pass list

    """
    lst = []

    for name in filter(lambda _n: check_detector_present(env, _n), aliases):
        # FIXME: somehow strings are encoded weird here
        det = psana.Detector(name.encode('utf-8'), env)

        par = partial(_process_ipm, det=det, name=name)
        lst.append(par)

    return lst


def _process_ipm(evt, det=None, name=None):
    # FIXME: workaround for not every ipm reporting channels
    return {name: dict(sum=det.sum(evt))}


def make_evrs(env, run, aliases=None):
    """adds functions to process evrs

    Args:
        env:
        run:

    Keyword Args:
        aliases: a list of evr aliases

    Returns:
        a list of analysis processing functions to add to the first_pass list

    """
    lst = []

    for name in filter(lambda _n: check_detector_present(env, _n), aliases):
        log.debug('constructing name {}'.format(name))
        det = psana.Detector(name, env)
        log.debug('constructing evr config for {}'.format(det.source))
        cfg = env.configStore().get(psana.EvrData.ConfigV7, psana.Source(det.source))
        all_codes = [c.code() for c in cfg.eventcodes()]

        par = partial(_process_evr, det=det, name=name, all_codes=all_codes)
        lst.append(par)

    return lst


def _process_evr(evt, det=None, name=None, all_codes=None):
    log.debug('running for evr name {}'.format(name))
    d = dict()
    evr_codes = det.eventCodes(evt, this_fiducial_only=True)

    if evr_codes is not None:
        for c in all_codes:
            d.update({'{}/code_{:d}'.format(name, c): 1 if c in evr_codes else 0})

    return d
