# stuff for find_droplets
import numpy as np
import scipy.ndimage.measurements as smt
from scipy.ndimage.filters import maximum_filter
from skimage.measure import regionprops


default_droplet_parameters = {'seed_threshold': 110/4., 'join_threshold': 10.}


def find_droplets(img, seed_threshold, join_threshold=0.0):
    """the droplet algorithm stolen from clemens

    Args:
        img:
        seed_threshold:
        join_threshold:

    Returns:

    """
    # footprint
    fp = np.array([[0, 1, 0],
                   [1, 1, 1],
                   [0, 1, 0]])

    droplets, ndroplets = smt.label(img > seed_threshold)
    if join_threshold > 0.0:
        droplets = maximum_filter(droplets, footprint=fp)
        droplets[img < join_threshold] = 0

    regions = regionprops(droplets, intensity_image=img)

    ndroplets = len(regions)
    x = []
    y = []
    adu = []
    npix = []
    moment = []
    for i, region in enumerate(regions):
        pos = region.weighted_centroid
        x.append(pos[0])
        y.append(pos[1])
        adu.append(region.intensity_image.sum())
        npix.append(int(region.area))
        moment.append(np.sqrt(region.weighted_moments_central[0, 2] + region.weighted_moments_central[2, 0]))
    return x, y, adu, npix, ndroplets, moment
