import straitlets
from traitlets import validate
from six import (print_, iterkeys, itervalues, iteritems)
from collections import namedtuple
import numpy as np


valid_hutch_names = ['amo','sxr','xpp','xcs','mfx','cxi','mec']


class AnalysisConfig(straitlets.Serializable):
    experiment_name = straitlets.Unicode(default_value='xpplr8116')
    hutch = straitlets.Enum(valid_hutch_names, default_value='xpp')
    run_list = straitlets.List(trait=straitlets.Integer, default_value=[1,])
    event_max = straitlets.Integer(default_value=None, allow_none=True)
    step_max = straitlets.Integer(default_value=None, allow_none=True)
    verbosity = straitlets.Integer(default_value=0)
    output_path = straitlets.Unicode(default_value='small_data.h5')
    gather_interval = straitlets.Integer(default_value=100)
    epics_vars = straitlets.List(trait=straitlets.Unicode)
    epix_cameras = straitlets.List(trait=straitlets.Unicode)
    cspad_cameras = straitlets.List(trait=straitlets.Unicode)
    zyla_cameras = straitlets.List(trait=straitlets.Unicode)
    rayonix_cameras = straitlets.List(trait=straitlets.Unicode)
    jungfrau_cameras = straitlets.List(trait=straitlets.Unicode)
    fee_spectrometers = straitlets.List(trait=straitlets.Unicode)
    ipms = straitlets.List(trait=straitlets.Unicode)
    evrs = straitlets.List(trait=straitlets.Unicode)
    droplet_parameters = straitlets.Dict()
    event_stride = straitlets.Integer(default_value=None, allow_none=True)
    roi_map = straitlets.Dict()
    rotation_spec = straitlets.Dict()
    transients_spec = straitlets.Dict()
    projection_spec = straitlets.Dict()
    reduction_spec = straitlets.Dict()
    threshold_spec = straitlets.Dict()
    save_keys = straitlets.List(trait=straitlets.Unicode)
    prune_keys = straitlets.List(trait=straitlets.Unicode)
    save_json = straitlets.Bool(default=False)
    batch_submit_string = straitlets.Unicode(allow_none=True, default_value=None)

    # TODO: put checks in here for things like (event_min > event_max) and gather_interval > event_max
    @validate('roi_map')
    def _valid_roi_map(self, proposal):
        for key,value in iteritems(proposal['value']):
            if type(value) != list:
                raise ValueError('roi_map keys must be a list. Check key {}'.format(key))

            for newname, spec in value:
                # check that ROI is sensible
                spec = np.array(spec)
                if spec.shape != (2, 2):
                    raise ValueError('roi_map spec must be 4 values. Check key {}/{}'.format(key, newname))

                if not all([spec[0, 0] <= spec[1, 0], spec[0, 1] <= spec[1, 1]]):
                    raise ValueError('roi_map spec results in zero size image. Check key {} -> {}'.format(key, newname))

        return proposal['value']


'''a tuple of dicts representing keys to update and delete after running a second pass function
'''
SecondPassResult = namedtuple('SecondPassResult', ['update', 'delete'])


