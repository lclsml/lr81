'''module for functional programming utilities
'''
from six import print_
import time

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()


from .utils import _fake_work

def mpi_map(fun, seq):
    """split a sequence into chunks based on the process rank

    Args:
        fun: a function that accepts an element of the input sequence ```seq```
        seq: a sequence

    Returns:
        a generator of values from function fun

    """
    for idx,item in enumerate(seq):
        if idx%size != rank: continue # skip processing on this node
        yield fun(item)


def test_mpi_map():
    import time
    import numpy as np

    def fake_work(value):
        print_('{rank:02d}/{size:02d}: processing {value:d}'.format(rank=rank, size=size, value=value))
        time.sleep(0.5)
        return value

    truth = np.arange(10)
    test = np.empty_like(truth)

    partial = np.array(list(mpi_map(_fake_work, truth)))

    comm.Gather(partial, test, root=0)

    if rank == 0:
        assert np.allclose(sorted(test), truth)


if __name__ == '__main__':
    test_mpi_map()
