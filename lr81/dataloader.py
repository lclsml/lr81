from six import (iterkeys, itervalues, iteritems)
import os
from functools import partial
from itertools import islice
import contextlib
import datetime
import logging
import json
import pathlib2 as pathlib
import warnings
import tables
import time

# extend environment before loading psana
SIT_CONFIG = dict(SIT_DATA='/reg/g/psdm/data', SIT_ROOT='/reg/g/psdm')
os.environ.update(SIT_CONFIG)
import psana

from .detectors import (process_default_detectors,
                        make_epics_recorder,
                        make_control_data,
                        make_evrs,
                        make_feespec_detectors,
                        make_ipms,
                        make_cspad,
                        make_epix100a,
                        make_jungfrau,
                        make_rayonix,
                        make_zyla)

from .algos import (apply_projections,
                    apply_rotations,
                    apply_reductions,
                    apply_rois,
                    apply_thresholds,
                    create_transient_keys,
                    dropletize_data,
                    save_nth,
                    prune_keys)

from .utils import (monitor_gather, get_memory_usage, experiment_files_copied)

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()


log = logging.getLogger('smalldata')
mpifmt = logging.Formatter(
    fmt='%(asctime)s [rank {rank}/{size}] %(name)s %(levelname)s: %(message)s'.format(rank=rank, size=size))


def process_shot(evt, lst=None):
    """process a single frame with a list of reductions

    Args:
        evt: an event object

    Keyword Args:
        lst: a list of functions that return a dict containing the result
            (name, value)

    Returns:
        a dictionary of the outputs of the reductions
    """

    data = {}
    if lst is None: return data

    for fun in lst:
        d = fun(evt)
        log.debug('processed keys {!s}'.format(d.keys()))
        data.update(d)

    return data


def process_data_dict(data, lst=None):
    """run a second-pass over the data dict to remove/generate data

    Args:
        datas: a dict from the output of process_shot

    Keyword Args:
        lst: a list of functions that mutate the input dict and return a tuple
            (dict of keys to update, list of keys to delete)

    Returns:
        a dictionary of the outputs of the reductions

    """
    if lst is None: return data

    for fun in lst:
        to_update, to_delete = fun(data)
        log.debug('updating keys {!s}'.format(list(iterkeys(to_update))))
        data.update(to_update)
        log.debug('removing keys {!s}'.format(list(iterkeys(to_delete))))

        for key in iterkeys(to_delete):
            del data[key]

    return data


def process_step_dict(data, stepidx, accumdata=None, lst=None):
    """accumulate data by merging the shot-by-shot dict to the accum dict

    Args:
        data: second-pass processed data
        stepidx: step number

    Keyword Args:
        accumdata: step data dict stored as {stepidx: {(data)}}
        lst: list of functions

    Returns:
        an updated accumdata dict. Since accumdata is a mutable object, it should be updated
        regardless of whether the return value is stored

    """
    accumdata = accumdata if accumdata else {}

    if lst is None:
        return accumdata

    stepdata = accumdata.get(stepidx, {})
    for fun in lst:
        to_update = fun(data, stepdata)
        log.debug('updating keys {!s}'.format(list(iterkeys(to_update))))
        stepdata.update(to_update)

    accumdata[stepidx] = stepdata

    return accumdata

def main(cfg):
    """run the analysis using the configuration object

    Args:
        cfg: an AnalysisConfig instance for running the analysis

    """

    # setup ignores for annoying pytables warnings
    warnings.simplefilter('ignore', tables.NaturalNameWarning)
    # disable h5py np.floating warning
    warnings.simplefilter('ignore', FutureWarning)

    hd = logging.StreamHandler()
    hd.setFormatter(mpifmt)
    log.addHandler(hd)

    if cfg.verbosity > 1:
        log.setLevel(logging.DEBUG)
    elif cfg.verbosity > 0:
        log.setLevel(logging.INFO)
    else:
        log.setLevel(logging.WARNING)

    if rank == 0:
        log.info('config: \n{}'.format(json.dumps(cfg.to_dict(), indent=2)))
        tstart = datetime.datetime.now()

    if len(cfg.run_list) > 1:
        log.fatal('cannot process multiple runs at the same time. Exiting...')
        return

    # wait for files to exist before starting processing
    while not experiment_files_copied(cfg):
        sleep_time = 30.0
        if rank == 0:
            log.info('waiting {sleep_time}s for files to copy'.format(sleep_time=sleep_time))

        time.sleep(sleep_time)

    # FIXME: hack that assumes single run for now
    run = cfg.run_list[0]

    run_str = ','.join(map(str, cfg.run_list))
    exp_str = 'exp={exp:s}:run={run:s}:smd'.format(exp=cfg.experiment_name,
                                                   run=run_str)
    ds = psana.MPIDataSource(exp_str)

    if cfg.event_max and (cfg.event_max > -1):
        ds.break_after(cfg.event_max)

    outp_path = cfg.output_path.format(**cfg.to_dict())
    p = pathlib.Path(outp_path).absolute()
    log.debug('final output path is `{}`'.format(p))

    nevent_log = [0] # used for monitor function
    # FIXME: bug with gather interval; if nevents < gather interval for a step, no gather happens
    # FIXME: workaround for now with gather_interval = cfg.gather_interval//size
    _store = ds.small_data(filename=str(p), gather_interval=max(cfg.gather_interval//size, 1))
    _store.add_monitor_function(partial(monitor_gather, nevent_log=nevent_log))

    log.debug('experiment string: {}'.format(exp_str))
    if cfg.save_json and rank == 0:
        json_path = p.with_name(p.with_suffix('').name + '-config').with_suffix('.json')
        log.debug('saving config to path `{}`'.format(json_path))

        with open(str(json_path), 'w') as f:
            json.dump(cfg.to_dict(), f, sort_keys=True, indent=2)

    # construct workflow here
    first_pass = []
    second_pass = []
    step_pass = []

    _process_default_detectors = partial(process_default_detectors, hutch=cfg.hutch)
    first_pass.append(_process_default_detectors)
    _evr_list = make_evrs(ds.env(), run, cfg.evrs)
    first_pass.extend(_evr_list)
    _control_data_list = make_control_data(ds.env(), run)
    first_pass.extend(_control_data_list)
    _feespec_list = make_feespec_detectors(ds.env(), run, aliases=cfg.fee_spectrometers)
    first_pass.extend(_feespec_list)
    _ipms_list = make_ipms(ds.env(), run, aliases=cfg.ipms)
    first_pass.extend(_ipms_list)
    _epics_var_list = make_epics_recorder(ds.env(), run, varlist=cfg.epics_vars)
    first_pass.extend(_epics_var_list)

    # add camera processing
    _epix_list = make_epix100a(ds.env(), run, cfg.epix_cameras)
    _cspad_list = make_cspad(ds.env(), run, cfg.cspad_cameras)
    _zyla_list = make_zyla(ds.env(), run, cfg.zyla_cameras)
    _jungfrau_list = make_jungfrau(ds.env(), run, cfg.jungfrau_cameras)
    _rayonix_list = make_rayonix(ds.env(), run, cfg.rayonix_cameras)
    first_pass.extend(_epix_list)
    first_pass.extend(_cspad_list)
    first_pass.extend(_zyla_list)
    first_pass.extend(_jungfrau_list)
    first_pass.extend(_rayonix_list)

    _create_transient_keys = partial(create_transient_keys, spec=cfg.transients_spec)
    second_pass.append(_create_transient_keys)

    _apply_thresholds = partial(apply_thresholds, spec=cfg.threshold_spec)
    second_pass.append(_apply_thresholds)


    _apply_rois = partial(apply_rois, roi=cfg.roi_map)
    second_pass.append(_apply_rois)

    _dropletize_data = partial(dropletize_data,
                                     camerakeys=cfg.droplet_parameters.keys(),
                                     droplet_parameters=cfg.droplet_parameters)
    second_pass.append(_dropletize_data)

    _apply_rotations = partial(apply_rotations, spec=cfg.rotation_spec)
    second_pass.append(_apply_rotations)

    _apply_projections = partial(apply_projections, spec=cfg.projection_spec)
    second_pass.append(_apply_projections)

    accumdata = {}
    _reduction_list = partial(apply_reductions, spec=cfg.reduction_spec)
    step_pass.append(_reduction_list)

    step_pass.append(partial(save_nth, keys=cfg.save_keys))

    # make sure pruning happens after the step_pass
    _prune_keys = partial(prune_keys, keylist=cfg.prune_keys)

    log.debug('starting run...')

    with contextlib.closing(_store) as store:
        # FIXME: bug here in use of islice: we never see the actual count when running in parallel,
        # so we actually get size*length of range selected
        for stepidx, step in enumerate(ds.steps()):
            log.debug('starting step {idx}'.format(idx=stepidx))
            if cfg.step_max is not None and (stepidx > cfg.step_max): break

            for ev in islice(step.events(), None, None, cfg.event_stride):
                log.debug('processing event {idx}'.format(idx=ds.nevent))

                data = process_shot(ev, first_pass)
                data = process_data_dict(data, second_pass)
                accumdata = process_step_dict(data, stepidx=stepidx, accumdata=accumdata, lst=step_pass)
                data = process_data_dict(data, [_prune_keys])

                store.event(data)

                log.debug('current memory usage %s', get_memory_usage())
                log.debug('data: %s', data) # use old-style logging here to avoid rendering data

            log.debug('saving accumulated data keys %s', accumdata.keys())

            # gather on every step just in case the gather interval is larger than number of events in a step
            store.gather()

        # preprocess keys in accumdata so that we can save it
        _accumdata = {'step_{:03d}'.format(k): v for k,v in iteritems(accumdata)}

        # FIXME: workaround for bug in MPIDataSource which allows final gather
        store.save({'step': _accumdata, 'cfg': cfg.to_json()})

    if rank == 0:
        tstop = datetime.datetime.now()
        log.info('job completed in {}.'.format(tstop - tstart))
