from six import print_
import sys
import click
from .types import AnalysisConfig
from .dataloader import main
import RegDB.experiment_info as experiment_info
import time
import pathlib2 as pathlib
import json
import shlex
import subprocess


@click.group()
def entrypoint():
    pass


@entrypoint.command()
@click.argument('exp', type=str)
@click.argument('runlist', type=str, default='1')
@click.option('--evmax', type=int, help='the last event to process from event list')
@click.option('--stepmax', type=int, help='the last step to process from step list')
@click.option('--stride', type=int, help='process every n-th event')
@click.option('-v', '--verbose', count=True, help='print more information to the log, can be used multiple times')
@click.option('-o', '--output-path', default='smalldata.h5', help='path to write the small data file')
@click.option('-n', '--gather-interval', default=100, help='number of events to process before gather operation')
@click.option('--epix-cameras', type=str, help='list of epix camera DAQ aliases')
@click.option('--cspad-cameras', type=str, help='list of cspad camera DAQ aliases')
@click.option('--zyla-cameras', type=str, help='list of zyla camera DAQ aliases')
@click.option('--jungfrau-cameras', type=str, help='list of jungfrau camera DAQ aliases')
@click.option('--rayonix-cameras', type=str, help='list of rayonix camera DAQ aliases')
@click.option('--fee-spectrometers', type=str, help='list of fee spectrometer DAQ aliases')
@click.option('-s', '--save', is_flag=True, expose_value=True, help='save a json config file')
@click.option('--epics', type=str, help='list of epics variables')
@click.option('--ipms', type=str, help='list of ipms')
@click.option('--evrs', type=str, help='list of evrs')
@click.option('--save-keys', type=str, help='list of keys to save once per step')
@click.option('--droplet-parameter', nargs=3, multiple=True, help='a tuple of alias name, seed threshold, and join threshold; can be used multiple times')
@click.option('--roi', nargs=6, multiple=True, help='region of interest specified as `camera_name/roi_name` itop jleft ibottom jright; can be used multiple times')
@click.option('--prune', type=str, help='comma-separated list of keys to remove before saving')
@click.option('--project', nargs=2, multiple=True, help='a tuple of 2d image key and projection name; may be used multiple times')
@click.option('--rotate', nargs=3, multiple=True, help='a tuple of image key, output name, and angle to rotate image; may be used multiple times')
@click.option('--reduce', nargs=2, multiple=True, help='a tuple of image key and reduction name; may be used multiple times')
@click.option('--threshold', nargs=3, multiple=True, help='a tuple of image key, threshold name, and param; may be used multiple times')
def manual(exp, runlist, verbose, evmax, stride, stepmax, output_path, gather_interval, epix_cameras,
        cspad_cameras, zyla_cameras, jungfrau_cameras, rayonix_cameras, fee_spectrometers,
        epics, ipms, evrs, save_keys, droplet_parameter, roi, project, reduce, threshold, rotate, prune, save):
    """run small data analysis with the pipeline configured through arguments

    Given an experiment name like "xpptut15" and a (comma-separated) list of runs,
    this script runs the "small data" analysis on that experiment. Parts of the pipeline
    can be plugged in by using the configuration options.
    """
    cfg = AnalysisConfig()

    # FIXME: this can go into the validation/coercion of the config instead of here
    runlist = list(map(int, runlist.split(',')))

    epix_cameras = [] if epix_cameras is None else epix_cameras.split(',')
    cspad_cameras = [] if cspad_cameras is None else cspad_cameras.split(',')
    zyla_cameras = [] if zyla_cameras is None else zyla_cameras.split(',')
    rayonix_cameras = [] if rayonix_cameras is None else rayonix_cameras.split(',')
    jungfrau_cameras = [] if jungfrau_cameras is None else jungfrau_cameras.split(',')
    fee_spectrometers = [] if fee_spectrometers is None else fee_spectrometers.split(',')
    epics_vars = [] if epics is None else epics.split(',')
    ipms = [] if ipms is None else ipms.split(',')
    evrs = [] if evrs is None else evrs.split(',')
    save_keys = [] if save_keys is None else save_keys.split(',')
    prune_keys = [] if prune is None else prune.split(',')

    if droplet_parameter is None:
        droplet_parameter = {}
    else:
        droplet_parameter = {item[0]: {'seed_threshold': float(item[1]), 'join_threshold': float(item[2])} for item in droplet_parameter}

    roi_map = {}
    if roi is not None:
        for _item in roi:
            name = _item[0]
            newname = _item[1]
            item = map(int, _item[2:])
            p = roi_map.get(name, [])
            p.append((newname, [[item[0], item[1]], [item[2], item[3]]]))
            roi_map[name] = p

    projection_spec = {}
    if project is not None:
        for _item in project:
            img_name = _item[0]
            proj_name = _item[1]
            p = projection_spec.get(img_name, None)
            if p is None:
                projection_spec[img_name] = []
                p = projection_spec[img_name]

            p.append(proj_name)

    reduction_spec = {}
    if reduce is not None:
        for _item in reduce:
            img_name = _item[0]
            proj_name = _item[1]
            p = reduction_spec.get(img_name, None)
            if p is None:
                reduction_spec[img_name] = []
                p = reduction_spec[img_name]

            p.append(proj_name)

    rotation_spec = {}
    if rotate is not None:
        for _item in rotate:
            img_name = _item[0]
            rot_name = _item[1]
            rot_angle = _item[2]
            p = rotation_spec.get(img_name, None)
            if p is None:
                rotation_spec[img_name] = []
                p = rotation_spec[img_name]

            p.extend([rot_name, rot_angle])

    threshold_spec = {}
    if threshold is not None:
        for _item in threshold:
            img_name = _item[0]
            threshold_name = _item[1]
            p = threshold_spec.get(img_name, None)
            if p is None:
                p = threshold_spec[img_name] = []

            p.extend([threshold_name, _item[2]])

    cfg.experiment_name = exp
    cfg.hutch = exp[:3]
    cfg.run_list = runlist
    cfg.event_max = evmax
    cfg.event_stride = stride
    cfg.step_max = stepmax
    cfg.verbosity = verbose
    cfg.output_path = output_path
    cfg.gather_interval = gather_interval
    cfg.epix_cameras = epix_cameras
    cfg.cspad_cameras = cspad_cameras
    cfg.zyla_cameras = zyla_cameras
    cfg.jungfrau_cameras = jungfrau_cameras
    cfg.rayonix_cameras = rayonix_cameras
    cfg.fee_spectrometers = fee_spectrometers
    cfg.epics_vars = epics_vars
    cfg.ipms = ipms
    cfg.evrs = evrs
    cfg.save_keys = save_keys
    cfg.droplet_parameters = droplet_parameter
    cfg.roi_map = roi_map
    cfg.rotation_spec = rotation_spec
    cfg.projection_spec = projection_spec
    cfg.reduction_spec = reduction_spec
    cfg.threshold_spec = threshold_spec
    cfg.transients_spec = {} # FIXME: actually do something with these keys
    cfg.prune_keys = prune_keys
    cfg.save_json = save

    main(cfg)


@entrypoint.command()
@click.argument('path')
@click.option('-r', '--runlist', type=str, help='override selected run with this one')
@click.option('-s', '--save', default=False, is_flag=True, help='save resulting config')
def cfg(path, runlist, save):
    with open(path) as f:
        try:
            _cfg = AnalysisConfig.from_json(f.read())
        except Exception as e:
            print_('ERROR: ', e.message)
            print_('ERROR: ', 'unable to process configuration file `{}`'.format(path))
            sys.exit(-1)

    if runlist:
        runlist = list(map(int, runlist.split(',')))
        _cfg.run_list = runlist

    _cfg.save_json = save

    main(_cfg)

@entrypoint.command()
@click.argument('template')
@click.option('-r','--reprocess', default=0)
@click.option('--sleep/--no-sleep', default=True)
def monitor(template, reprocess, sleep):
    with open(template) as f:
        try:
            _cfg = AnalysisConfig.from_json(f.read())
        except Exception as e:
            print_('ERROR: ', e.message)
            print_('ERROR: ', 'unable to process configuration file `{}`'.format(template))
            sys.exit(-1)

    _cfg.save_json = False

    if _cfg.batch_submit_string is None:
        print_('ERROR: null batch submit string, cannot automatically process')
        sys.exit(-1)

    batch_submit_string_template = _cfg.batch_submit_string
    output_path_template = _cfg.output_path

    completed_runs = experiment_info.experiment_runs(_cfg.hutch.upper(), _cfg.experiment_name)
    print_('Found {nruns} completed runs in experiment {experiment}. Reprocessing last {reprocess} runs.'.format(nruns=len(completed_runs), experiment=_cfg.experiment_name, reprocess=reprocess))

    sleep_time = 60 if sleep else 0

    lastcount = max(len(completed_runs) - reprocess, 0)

    cfg_path = pathlib.Path('cfgs')
    log_path = pathlib.Path('logs')

    cfg_path.mkdir(exist_ok=True)
    log_path.mkdir(exist_ok=True)

    # start poll loop
    while True:
        try:
            completed_runs = experiment_info.experiment_runs(_cfg.hutch.upper(), _cfg.experiment_name)
            currcount = len(completed_runs)

            if lastcount < currcount:
                # write out configuration file to path
                _cfg.run_list = [lastcount + 1]
                outp_path = output_path_template.format(**_cfg.to_dict())
                _cfg.output_path = outp_path
                p = pathlib.Path(outp_path)
                cp = cfg_path / p.with_name(p.with_suffix('').name + '-config').with_suffix('.json').name

                _cfg.batch_submit_string = batch_submit_string_template.format(cfg_path=str(cp), **_cfg.to_dict())

                with cp.open('wb') as f:
                    json.dump(_cfg.to_dict(), f, sort_keys=True, indent=2)

                args = shlex.split(_cfg.batch_submit_string)
                print_('Found new job, run {}. Sleeping for a bit...'.format(_cfg.run_list[0]))
                time.sleep(sleep_time)
                print_('Submitting job ...')
                print_(subprocess.check_output(args))
                lastcount += 1
            else:
                time.sleep(5.0)

        except KeyboardInterrupt:
            print_('Got keyboard interrupt...')
            break


if __name__ == '__main__':
    entrypoint()
