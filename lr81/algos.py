from six import (iterkeys, itervalues, iteritems)
from functools import partial
import operator
import copy
import numpy as np
import logging
from scipy.ndimage import rotate

from .types import SecondPassResult
from .droplet import (find_droplets, default_droplet_parameters)

log = logging.getLogger('smalldata')

def threshold(a, threshmin=None, threshmax=None, newval=0):
    """re-insert scipy.stats.threshold that was deprecated in 1.0

    Clip array to a given value.
    Similar to numpy.clip(), except that values less than `threshmin` or
    greater than `threshmax` are replaced by `newval`, instead of by
    `threshmin` and `threshmax` respectively.
    Parameters
    ----------
    a : array_like
        Data to threshold.
    threshmin : float, int or None, optional
        Minimum threshold, defaults to None.
    threshmax : float, int or None, optional
        Maximum threshold, defaults to None.
    newval : float or int, optional
        Value to put in place of values in `a` outside of bounds.
        Defaults to 0.
    Returns
    -------
    out : ndarray
        The clipped input array, with values less than `threshmin` or
        greater than `threshmax` replaced with `newval`.
    Examples
    --------
    >>> a = np.array([9, 9, 6, 3, 1, 6, 1, 0, 0, 8])
    >>> from scipy import stats
    >>> stats.threshold(a, threshmin=2, threshmax=8, newval=-1)
    array([-1, -1,  6,  3, -1,  6, -1, -1, -1,  8])
    """
    a = np.asarray(a).copy()
    mask = np.zeros(a.shape, dtype=bool)
    if threshmin is not None:
        mask |= (a < threshmin)
    if threshmax is not None:
        mask |= (a > threshmax)
    a[mask] = newval
    return a


def dropletize_data(data, camerakeys=None, remove_original=False, droplet_parameters=None):
    """apply the find_droplet algorithm to each image in the imagekeys list

    Args:
        data: a dict of data obtained from the first pass

    Keyword Args:
        camerakeys: list of camera names e.g. [Epix100a, Cspad2x2] that have an image key (Epix100a/image)
        remove_original: add the image keys to the delete list after running

    Returns:
        a SecondPassResult representing keys to update or remove from the data dict

    """
    if camerakeys is None: return SecondPassResult(update={}, delete={})

    if droplet_parameters is None:
        droplet_parameters = {}

    to_update, to_delete = {}, {}

    for key in camerakeys:
        img = data.get('{camera}/image'.format(camera=key), None)

        if img is None:
            log.warning('image key `{}` not found, skipping in dropletize_data'.format(key))
            continue

        params = droplet_parameters.get(key, default_droplet_parameters)

        log.debug('dropletizing {cameraname}/image'.format(cameraname=key))
        x, y, adu, npix, ndroplets, moment = tuple(map(np.array, find_droplets(img, **params)))
        to_update.update({'%s' % key: dict(ragged_x=x,
                                           ragged_y=y,
                                           ragged_adu=adu,
                                           ragged_npix=npix,
                                           ragged_ndroplets=ndroplets,
                                           ragged_moment=moment)})

        if remove_original:
            to_delete.update({key: None})

    return SecondPassResult(update=to_update, delete=to_delete)


def apply_rois(data, roi=None):
    """apply rois to images

    ```apply_rois``` creates regions of interest for a particular camera image.
    These regions are specified by the ```data``` dict, which has the form
    {'camera_name/roi_name': [[i_top, j_left], [i_bottom, j_right]]}. After the run,
    the key of the dict will contain the roi data in the pixel array.

    Args:
        data: a dict of data obtained from the first pass containing e.g. Epix100a/image key

    Keyword Args:
        roi: a regions of interest spec described above

    Returns:
        a SecondPassResult representing keys to update or remove from the data dict

    """
    to_update = {}
    to_delete = {}

    for imgkey, roilst in iteritems(roi):
        if imgkey not in data.keys():
            log.warning('key `{}` not found in data'.format(imgkey))
            continue

        img = data[imgkey]

        for roiname, spec in roilst:
            spec = np.array(spec, dtype=int)
            isel = slice(spec[0, 0], spec[1, 0]+1)
            jsel = slice(spec[0, 1], spec[1, 1]+1)

            to_update[roiname] = img[isel, jsel]

    return SecondPassResult(update=to_update, delete=to_delete)


def create_transient_keys(data, spec=None):
    """create a set of keys, filtered on a set of boolean keys, shot by shot

    Args:
        data: shot-by-shot event data

    Keyword Args
        spec: dict of {'existing_selected_key': ['name_if_true', 'existing_key,existing_key']}

    Returns:
        updated data, with name existing_selected_key_groupname_transient

    """
    to_update = {}

    for key, (groupname, flagspec) in iteritems(spec):
        if key not in data.keys():
            log.warning('unknown key {}, cannot create group'.format(key))
            continue

        cond = [data.get(cond_key, False) for cond_key in flagspec.split(',').strip()]
        if not all(cond):
            log.debug('condition %s is false for %s', flagspec, key)
            continue

        # assume condition is true
        newkey = '{key}_{groupname}_transient'.format(key=key, groupname=groupname)
        to_update[newkey] = data[key]
        log.debug('created group %s', groupname)

    return SecondPassResult(update=to_update, delete={})


def apply_reductions(data, stepdata=None, spec=None):
    """processes a list of reductions from a spec of {'data_key': ['reduction',]}

    Args:
        data: shot-by-shot event data
        stepdata: step-accumulated data

    Keyword Args:
        spec: dict of {'key_name_in_data': ['reduction_name']}

    Returns:
        an updated dictionary object

    """
    updated = copy.copy(stepdata) if stepdata else {}

    for key, rlist in iteritems(spec):
        if key not in data.keys():
            if 'transient' not in key: # do not warn about intermittent keys from grouping
                log.warning('unknown key {}, cannot reduce'.format(key))
            continue

        for name in rlist:
            fun = step_reduction_map.get(name, None)
            if fun is None:
                log.warning('unknown reduction {} for key {}'.format(name, key))
                continue

            outkey = '{shotname}_{reduction}'.format(shotname=key, reduction=name)
            log.debug('applying reduction %s', outkey)
            accum = updated.get(outkey, None)

            if accum is None:
                log.debug('initialized reduction %s', outkey)
                updated[outkey] = data[key]
                continue

            log.debug('updating reduction %s', outkey)
            updated[outkey] = fun(data[key], accum)

    return updated


def save_nth(data, stepdata=None, keys=None, pickn=0):
    """save keys from each step

    Args:
        data:
        stepdata:
        spec:
        pickn:

    Returns:

    """
    updated = copy.copy(stepdata) if stepdata else {}

    for key in keys:
        if key not in data.keys():
            log.warning('unknown key {}, cannot save'.format(key))
            continue

        outkey = '{shotname}_ev{nth:03d}'.format(shotname=key, nth=pickn)
        log.debug('saving nth %s', outkey)
        accum = updated.get(outkey, None)

        if accum is None:
            updated[outkey] = data[key]
            continue

    return updated


def apply_rotations(data, spec=None):
    """

    Args:
        data: a dict of data obtained from first pass

    Keyword Args:
        spec: dict containing {'image_name': [('output_name', angle), ...]}

    Returns:
        a SecondPassResult with keys to update or remove from data dict
    """
    to_update = {}
    for key, rot in iteritems(spec):
        img = data.get(key, None)
        if img is None:
            log.warning('unknown key {}, unable to rotate'.format(key))
            continue

        for name,angle in rot:
            to_update.update({name: rotate(img, angle, reshape=False)})

    return SecondPassResult(update=to_update, delete={})


def apply_projections(data, spec=None):
    """project images given in the spec

    Args:
        data: a dict of data obtained from the first pass

    Keyword Args:
        spec: dict containing {'image_name': ['projection1', 'projection2']}

    Returns:
        a SecondPassResult representing keys to update or remove from the data dict

    """
    to_update = {}
    for key, proj in iteritems(spec):
        img = data.get(key, None)
        if img is None:
            log.warning('unknown key {}, unable to project'.format(key))
            continue

        for name in proj:
            fun = projection_map.get(name, None)

            if fun is None:
                log.warning('unknown projection {name}, unable to project {image}.{name}'.format(name=name, image=key))
                continue

            k = key + '_' + name
            to_update.update({k: fun(img)})

    return SecondPassResult(update=to_update, delete={})


def apply_thresholds(data, spec=None):
    """threshold images given in the spec

    Args:
        data: a dict of data obtained from the first pass

    Keyword Args:
        spec: dict containing {'image_name': ['threshold_name', param1]}

    Returns:
        a SecondPassResult representing keys to update or remove from the data dict

    """
    to_update = {}
    for key, thresh_spec in iteritems(spec):
        img = data.get(key, None)
        if img is None:
            log.warning('unknown key {}, unable to project'.format(key))
            continue

        name, param1 = thresh_spec

        fun = threshold_map.get(name, None)

        if fun is None:
            log.warning('unknown projection {name}, unable to project {image}.{name}'.format(name=name, image=key))
            continue

        k = key + '_' + name
        to_update.update({k: fun(img, float(param1))})

    return SecondPassResult(update=to_update, delete={})


def prune_keys(data, keylist=None):
    """clean up temporary keys

    Args:
        data: a dict of data obtained from the first pass containing e.g. Epix100a/image key

    Keyword Args:
        keylist: a list of keys to remove

    Returns:
        a SecondPassResult representing keys to update or remove from the data dict

    """
    keylist = set(keylist)
    notfound = keylist.difference(data.keys())
    if notfound:
        log.warning('unable to prune {} from data, keys not present'.format(list(notfound)))

    to_delete = {k: None for k in keylist.intersection(data.keys())}
    log.debug('pruning {} from data'.format(to_delete.keys()))

    return SecondPassResult(update={}, delete=to_delete)


projection_map = dict(sumi=partial(np.nansum, axis=0),
                      sumj=partial(np.nansum, axis=1),
                      sum=np.nansum)

threshold_map = dict(clip=partial(threshold, threshmax=None, newval=0))

step_reduction_map = dict(stepsum=operator.add)
