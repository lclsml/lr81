import time
from itertools import islice
import logging
from six import print_
import resource
from .pptree import (Node, print_tree)

log = logging.getLogger('smalldata')

try:
    from RegDB import experiment_info
    import pathlib2 as pathlib
except ImportError:
    log.warn('cannot access experiment info')
    experiment_info = None


def get_memory_usage():
    memkb = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

    for suf in ['Kb', 'Mb', 'Gb']:
        if memkb < 1024.0:
            return '{sz:3.1f} {suf}'.format(sz=memkb, suf=suf)
        memkb /= 1024.0

    return '{sz:3.1f} {suf}'.format(sz=memkb, suf='Tb')


def monitor_gather(data, nevent_log=None):
    mem = get_memory_usage()
    nkeys = len(data)
    nevents = len(data.get('fiducials'))

    currev = 'unk' if nevent_log is None else nevent_log[0] + nevents

    log.info('Gathered {nevents} events with {nkeys} keys, {currev} shots seen'.format(nevents=nevents, nkeys=nkeys, currev=currev))
    log.info('Current memory usage {}'.format(mem))

    if nevent_log: nevent_log[0] = currev


def pretty_print_keys(h5file, filter=''):
    """pretty print the generated keys

    Args:
        h5file:

    Returns:
        None

    """
    root = Node('root')
    nodes = {}

    def visit(name, obj, nodes=nodes):
        parent = nodes.get(obj.parent, root)

        name = name.split('/')[-1]
        node = Node(name, parent=parent)
        nodes[obj] = node

    h5file.visititems(visit)

    print_tree(root)


def experiment_files_copied(cfg):
    if experiment_info is None:
        raise RuntimeError('cannot access experiment info')

    expid = experiment_info.name2id(cfg.experiment_name)
    runnum = cfg.run_list[0]

    offline_path = "/reg/d/psdm/{hutch}/{experiment_name}/xtc/{filename}"
    fileinfo = experiment_info.get_open_files(expid, runnum)

    for f in fileinfo:
        p = pathlib.Path(f['dirpath'])
        outpath = pathlib.Path(offline_path.format(filename = str(p.name), **cfg.to_dict()))
        log.debug('searching for file {}'.format(outpath))

        if not outpath.exists():
            log.debug('file not found {}'.format(outpath))
            return False

    log.debug('all files present')
    return True

'''
# FIXME: decide whether this stuff is worth keeping
def _cumsum(seq):
    lst = []
    cum = 0
    for val in seq:
        cum += val
        lst.append(cum)

    return lst

def _fake_work(value, delay=0.5):
    print_('{rank:02d}/{size:02d}: processing {value:d}'.format(rank=rank, size=size, value=value))
    time.sleep(delay)
    return value


def make_mpi_map_offsets_sizes(chunksize):
    """calculate the sizes and offsets of returns from mpi_map call

    Args:
        chunksize: number of items that was in sequence for mpi_map
        mpisize: number of mpi processes in map

    Returns:
        two tuples, containing final sizes and offsets into receiving array
    """
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    mpirank = comm.Get_rank()
    mpisize = comm.Get_size()

    def _islice(start):
            return islice(range(chunksize), start, chunksize, mpisize)

    sz = map(len, map(list, map(_islice, range(mpisize))))
    offs = [0]
    offs.extend(_cumsum(sz))
    offs = offs[:-1]

    return tuple(sz), tuple(offs)


def test_cumsum():
    lst = list(range(10))
    print_(_cumsum(lst))


def test_make_mpi_map_offsets_sizes():
    chunksize = 10

    print_(make_mpi_map_offsets_sizes(chunksize, mpisize=1))
    print_(make_mpi_map_offsets_sizes(chunksize, mpisize=3))


if __name__ == '__main__':
    test_cumsum()
    test_make_mpi_map_offsets_sizes()
'''
