from six import print_
import toolz
import numpy as np

from lr81.functional import mpi_map, _fake_work
from lr81.utils import make_mpi_map_offsets_sizes

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

chunksize = 10
data = np.arange(1, 101)

fake_work = toolz.curry(_fake_work, delay=0.1)

for chunk in toolz.partition_all(chunksize, data):
    partial = np.array(list(mpi_map(fake_work, chunk)))

    # estimate chunk size
    sz, offs = make_mpi_map_offsets_sizes(len(chunk))

    if rank == 0:
        recv = np.empty((chunksize,), dtype=int)
    else:
        recv = None

    comm.Gatherv(partial, [recv, sz, offs, MPI.INT64_T], root=0)

    if rank == 0:
        print_('Got buffers:')
        print_(recv)
