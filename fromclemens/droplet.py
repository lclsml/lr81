import time
import datetime
import argparse
import h5py
import psana
import PSCalib.GlobalUtils as gu
import numpy as np
from mpi4py import MPI
import scipy.ndimage.measurements as smt
from scipy.ndimage.filters import maximum_filter
from skimage.measure import regionprops

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

experiment = 'mfxlq3915'
ffb = True

parser = argparse.ArgumentParser(description='Dropletize Epix100 detector')
parser.add_argument('-r','--run', help='run number', required=True, type=int)
args = parser.parse_args()

fp = np.array([[0, 1, 0],
               [1, 1, 1],
               [0, 1, 0]])

def find_droplets(img, seed_threshold, join_threshold=0.0):
    droplets, ndroplets = smt.label(img > seed_threshold)
    if join_threshold > 0.0:
        droplets = maximum_filter(droplets, footprint=fp)
        droplets[img < join_threshold] = 0

    regions = regionprops(droplets, intensity_image=img)

    ndroplets = len(regions)
    x = []
    y = []
    adu = []
    npix = []
    moment = []
    for i, region in enumerate(regions):
        pos = region.weighted_centroid
        x.append(pos[0])
        y.append(pos[1])
        adu.append(region.intensity_image.sum())
        npix.append(int(region.area))
        moment.append(np.sqrt(region.weighted_moments_central[0, 2] + region.weighted_moments_central[2, 0]))
    return x, y, adu, npix, ndroplets, moment

def gather_array(values):
    sizes = comm.gather(values.shape[0])
    recv = None
    if rank == 0:
        recv = np.empty(sum(sizes), dtype=values.dtype)
    comm.Gatherv(sendbuf=values, recvbuf=[recv, sizes])
    return recv

exp_string = 'exp=%s:run=%d:smd' %(experiment, args.run)
if ffb:
    exp_string += ':dir=/reg/d/ffb/%s/%s/xtc:live' %(experiment[:3], experiment)

if rank == 0:
    print exp_string
ds = psana.DataSource(exp_string)
det = psana.Detector('Epix100a')
fee_gas_det = psana.Detector('FEEGasDetEnergy')
mask = det.mask(args.run, calib=True, status=False, edges=True, central=True, unbond=True, unbondnbrs=True)
evr = psana.Detector('evr0')

avail_detectors = zip(*psana.DetNames('detectors'))[0]

# check if gain file really exists
gain = det.gain(args.run)
status = det.loading_status(args.run, gu.PIXEL_GAIN)
if status != 1:
    print 'Can not find gain file. Aborting !'
    comm.Abort(-1)

keys = [('x', np.float32),
        ('y', np.float32),
        ('adu', np.float32),
        ('npix', np.uint32),
        ('ndroplets', np.uint32),
        ('moment', np.float32),
        ('gmd', np.float32),
        ('time', np.int64),
        ('fiducial', np.int32),
        ('xray_status', np.uint8)]
data = {}
dtype = {}
for key, dt in keys:
    data[key] = []
    dtype[key] = dt

start_time = datetime.datetime.now()
for i, evt in enumerate(ds.events()):
    #if i > 4000:
    #    break

    if i%size != rank:
        continue

    img = det.calib(evt)
    if img is None:
        continue

    img *= mask

    fee_gas = fee_gas_det.get(evt)
    if fee_gas is None:
        pe = -1.0
    else:
        pe = 0.5*(fee_gas.f_21_ENRC() + fee_gas.f_22_ENRC())

    evt_id = evt.get(psana.EventId)
    sec, nsec = evt_id.time()
    event_codes = evr.eventCodes(evt)
    if event_codes is None:
        continue

    x, y, adu, npix, ndroplets, moment = find_droplets(img, 110/4.0, 10.0)
    if len(adu) > 0:
        data['x'].extend(x)
        data['y'].extend(y)
        data['adu'].extend(adu)
        data['npix'].extend(npix)
        data['moment'].extend(moment)

        data['ndroplets'].append(ndroplets)
        data['gmd'].append(pe)
        
        # convert time to matching string from cctbx 
        data['time'].append(time.strftime('%Y%m%d%H%M%S', time.gmtime(sec)) + ('%03d' % (nsec // 1000000)))
        #data['time'].append(int((sec<<32)|nsec))
        data['fiducial'].append(evt_id.fiducials())
        if 162 in event_codes:
            data['xray_status'].append(0)
        else:
            data['xray_status'].append(1)

# gather arrays from all ranks
for key in data.keys():
    data[key] = gather_array(np.array(data[key], dtype=dtype[key]))

end_time = datetime.datetime.now()
if rank == 0:
    run_time = (end_time - start_time).total_seconds()
    nevents = data['ndroplets'].shape[0]-1
    print 'Processed %d events in %d seconds' %(nevents, run_time)
    print 'Processing rate %d Hz' %int(nevents / run_time)
    with h5py.File('/reg/d/psdm/%s/%s/results/xes/run-%03d-new-gain.h5' %(experiment[:3], experiment, args.run), 'w') as fh:
        for key, value in data.iteritems():
            fh.create_dataset(key, data=value, compression='lzf')
