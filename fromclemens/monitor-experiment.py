import re
import glob
import time
import shlex
from subprocess import check_call
import RegDB.experiment_info as experiment_info

def get_run_number():
    sorted(glob.glob('/reg/d/psdm/mfx/mfxn8416/xtc/e925-*-s00-c00.xtc'))[-1]
    return int(re.findall('r(\d+)', s)[0])

current_run = int(experiment_info.experiment_runs('MFX')[-1]['num'])

print 'Start looking for new runs'

while True:
    next_run = int(experiment_info.experiment_runs('MFX')[-1]['num'])
    if next_run > current_run:
        print 'New run:', next_run
        check_call(shlex.split('bsub -q psfehhiprioq -o logs/log-r{run}-\%j.out -n 32 mpirun python droplet.py -r {run}'.format(run=next_run)))
        print 'Job submitted at', time.ctime()
        current_run = next_run
    time.sleep(5.0)
