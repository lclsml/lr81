from six import iterkeys, iteritems, itervalues, print_
import psana
import itertools as it
import numpy as np
import os
from lr81.types import AnalysisConfig
from lr81.utils import experiment_files_copied

print_(os.uname()[1])

ds = psana.DataSource('exp=mfxls4916:run=17')
env = ds.env()
run = next(ds.runs())

# let's grab a few events to play with
events = list(it.islice(ds.events(), 10))

ev = events[0]
print_(ev.keys())

print_(ev.get(psana.EventId))

#with open('cfgs/xcsls5816-day1.json', 'r') as f:
#    cfg = AnalysisConfig.from_json(f.read())

#print_(experiment_files_copied(cfg))


